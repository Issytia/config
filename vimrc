set viminfo+=n$XDG_CONFIG_HOME/vim/viminfo
let $VIM_HOME=fnamemodify($MYVIMRC, ':h')
set runtimepath=$VIM_HOME,$VIM_HOME/after,$VIM,$VIMRUNTIME

set number relativenumber
if &t_Co > 1
	syntax enable
endif
filetype plugin indent on
set tabstop=4 shiftwidth=4 noexpandtab softtabstop=0 smarttab
autocmd FileType yaml setlocal noexpandtab
