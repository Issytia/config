function fish_prompt
    #Save the return status of the previous command
    set -l last_pipestatus $pipestatus
    set -lx __fish_last_status $status # Export for __fish_print_pipestatus.

    if not set -q VIRTUAL_ENV_DISABLE_PROMPT
        set -g VIRTUAL_ENV_DISABLE_PROMPT true
    end

    if type -q termux-info
        set_color magenta
        echo -n (cat ~/.devicename)
    else
        set_color yellow
        printf '%s' $USER
        set_color normal
        printf ' at '

        set_color magenta
        if set -q CHROOT
            echo -n $CHROOT
            set_color red
            echo -n ' (chroot)'
        else
            echo -n (prompt_hostname)
        end
    end
    set_color normal
    printf ' in '

    set_color $fish_color_cwd
    printf '%s' (prompt_pwd)
    set_color normal

    printf ' [%s] %s' (date "+%H:%M:%S") (__fish_print_pipestatus "" "" "|" (set_color $fish_color_status) (set_color --bold $fish_color_status) $last_pipestatus)

    # Line 2
    echo
    if test -n "$VIRTUAL_ENV"
        printf "(%s) " (set_color blue)(basename $VIRTUAL_ENV)(set_color normal)
    end
    if functions -q fish_is_root_user; and fish_is_root_user
        printf '# '
    else
        printf '↪ '
    end
    set_color normal
end
