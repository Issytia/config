nnoremap <buffer> <silent> C :Cycle<CR>
nnoremap <buffer> <silent> S :Squash<CR>
nnoremap <buffer> <silent> E :Edit<CR>
nnoremap <buffer> <silent> R :Reword<CR>
nnoremap <buffer> <silent> F :Fixup<CR>
