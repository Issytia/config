#!/usr/bin/env -S perl -w

my @root;

BEGIN {
	use strict; use warnings;

	my @root_options = (
		{ 'exe' => 'doas', 'cmd' => ['doas'] },
		{ 'exe' => 'sudo', 'cmd' => ['sudo'] },
		{ 'exe' => 'su', 'cmd' => ['su', 'root', '--'] }
	);
	for my $option (@root_options) {
		if (system('which ' . $option->{'exe'} . ' > /dev/null 2>&1') == 0) {
			@root = @{$option->{'cmd'}};
			last;
		}
	}
	unless (@root) {
		warn "no sudo equivalent found\n";
		exit 1;
	}
	my @mods = ('File::Touch', 'File::Slurp', 'List::Pairwise', 'Term::ReadKey', 'TOML', 'TOML::Parser');
	my @missing;
	my @cmds = (
		{ 'cmd' => ['yay', '-S'], 'pkg' => sub { 'perl-' . lc(shift =~ s/::/-/gr) }, 'root' => 0 },
		{ 'cmd' => ['pacman', '-S'], 'pkg' => sub { 'perl-' . lc(shift =~ s/::/-/gr) }, 'root' => 1 },
		{ 'cmd' => ['dnf', 'install'], 'pkg' => sub { 'perl(' . (shift) . ')' }, 'root' => 1 },
		{ 'cmd' => ['apt', 'install'], 'pkg' => sub { 'lib' . lc(shift =~ s/::/-/gr) . '-perl' }, 'root' => 1 },
		{ 'cmd' => ['cpanm'], 'pkg' => sub { $_ }, 'root' => 0 }
	);

	for my $mod (@mods) {
		eval "use $mod";
		push @missing, $mod if $@ ne "";
	}

	if (@missing) {
		my @cmd;

		for my $option (@cmds) {
			if (system('which ' . $option->{'cmd'}[0] . ' > /dev/null 2>&1') == 0) {
				if ($option->{'root'}) {
					@cmd = (@root, @{$option->{'cmd'}});
				} else {
					@cmd = @{$option->{'cmd'}};
				}

				if (@cmd) {
					push @cmd, map { $option->{'pkg'}->($_) } @missing;
					last;
				}
			}
		}

		if (not @cmd) {
			warn "no package manager found\n";
			exit 1;
		}

		print "installing missing modules...\n";
		print '    ' . join(' ', @cmd) . "\n";
		system @cmd;

		my $fail = 0;
		for my $mod (@missing) {
			eval "use $mod";
			if ($@ ne "") {
				warn "still missing:\n" unless $fail;
				warn "    $mod\n";
				$fail = 1;
			}
		}

		exit 1 if $fail;
	}
}

local $TOML::PARSER = TOML::Parser->new(
	inflate_boolean => sub { $_[0] eq 'true' ? 1 : 0 }
);

use Cwd qw(abs_path getcwd);
use File::Basename qw(basename dirname);
use File::Compare;
use File::Fetch;
use File::Spec::Functions qw(abs2rel catdir catfile file_name_is_absolute splitdir rel2abs);
use List::Util qw(max);
use Sys::Hostname qw(hostname);

File::Slurp->import('slurp');

$ENV{"HOME"} = (getpwuid($<))[7] unless exists $ENV{"HOME"};
$ENV{"XDG_CONFIG_HOME"} = catdir($ENV{"HOME"}, ".config") unless exists $ENV{"XDG_CONFIG_HOME"};
$ENV{"XDG_DATA_HOME"} = catdir($ENV{"HOME"}, ".local", "share") unless exists $ENV{"XDG_DATA_HOME"};
$ENV{"XDG_CACHE_HOME"} = catdir($ENV{"HOME"}, ".cache") unless exists $ENV{"XDG_CACHE_HOME"};
$ENV{"XDG_RUNTIME_DIR"} = catdir($ENV{"HOME"}, ".run") unless exists $ENV{"XDG_RUNTIME_DIR"};

my $base = dirname dirname abs_path __FILE__;
my $bin = catdir($base, 'bin');

my $dev = -e catfile($base, '.dev');
if ($dev) {
	require Data::Dump;
	Data::Dump->import('pp');
}

my $hostname;
if (exists $ENV{'CHROOT'}) {
	$hostname = lc $ENV{'CHROOT'};
} elsif (system('which termux-info > /dev/null 2>&1') == 0) {
	$hostname = lc((slurp(catfile($ENV{'HOME'}, '.devicename')) or die "no ~/.devicename\n") =~ s/^\s*([^\s]+)\s*$/$1/r);
} else {
	$hostname = lc(hostname() =~ s/\..*$//r);
}

my $cache = catdir($base, 'cache');
my $backup = catdir($cache, 'backup');
my $linkscache = catfile($cache, 'links');
my $metafile = catfile($cache, 'meta.pl');
my $perlpp = catfile($cache, 'PerlPP.pm');
my $tagsfile = catfile($cache, 'tags');
my $mtime = (stat __FILE__)[9];
my $metatime = max((stat $perlpp)[9], (stat $tagsfile)[9], (stat $metafile)[9], $mtime);
my %links;

system 'mkdir', '-vp', $backup;

my %tags = List::Pairwise::mapp { lc $a => $b } %{from_toml(scalar slurp($tagsfile))};
my @post;

require $perlpp;

sub _abs {
	my $file = $_[0];
	my @dir = splitdir dirname $file;
	my $i = $#dir;
	for (; $i >= 0; --$i) {
		last if -d catdir(@dir[0 .. $i]);
	}
	return catfile(abs_path(catdir(@dir[0 .. $i])), @dir[$i+1 .. $#dir], basename $file);
}

update();
exec $^X, $0, @ARGV if $mtime < (stat __FILE__)[9];

my %cache;

sub update_cache {
	my ($rel, $full) = @_;
	my $parent = catfile($cache, $rel);

	if (not exists $cache{$rel}) {
		$cache{$rel} = {
			'time' => [0, 0],
			'depends' => []
		};
	}
	my $text = slurp($full) or do { warn "meta slurp failed for $rel: $!\n"; return; };
	my ($data, $err) = TOML::from_toml($text);
	unless ($data) {
		warn "from_toml failed for $rel: $err\n";
		system 'rm', '-vf', $parent, $full;
		return;
	}
	unless (exists $data->{'destination'}) {
		warn "missing destination for $rel\n";
		system 'rm', '-vf', $parent, $full;
		return;
	}

	$data->{'destination'} =~ s@^~([^/]*)@$1 ? (getpwnam($1))[7] // '~' . $1 : $ENV{HOME} // (getpwuid($<))[7]@e;
	$data->{'destination'} =~ s@\$([a-zA-Z_][a-zA-Z0-9_]*)@$ENV{$1} // ''@ge;
	$data->{'destination'} = catfile($data->{'destination'}, basename $rel) if $data->{'destination'} =~ m@/$@;
	$data->{'destination'} = _abs(rel2abs($data->{'destination'}, $base));

	$cache{$rel}{'time'}[1] = (stat $full)[9];
	@{$cache{$rel}}{keys %$data} = values %$data;
}

{
	my @dirs = ('');

	for my $dir (@dirs) {
		my $fulldir = catdir($cache, $dir);
		opendir my $dh, $fulldir or do { warn "can't open dir $dir: $!\n"; next; };
		while (my $file = readdir $dh) {
			my $rel = $dir eq '' ? $file : catfile($dir, $file);
			my $full = catfile($fulldir, $file);
			next if -d $full && ($file =~ m@^\.{1,2}$@ || $full eq $backup || $file eq '.git');
			next if !-d $full && ($file eq 'links' || $rel eq 'PerlPP.pm' || $rel eq 'meta.pl' || $rel eq 'tags');

			if (-d $full) {
				push @dirs, $rel;
			} else {
				if ($rel =~ m@^(.*)\.meta$@) {
					update_cache($1, $full);
				} else {
					if (not exists $cache{$rel}) {
						$cache{$rel} = {
							'time' => [0, 0],
							'depends' => []
						};
					}
					$cache{$rel}{'time'}[0] = (stat $full)[9];
				}
			}
		}
		closedir $dh;
	}
}

for my $rel (keys %cache) {
	my $basefile = catfile($base, $rel);
	my $cachefile = catfile($cache, $rel);
	my $metafile = "$cachefile.meta";
	my $backupfile = catfile($backup, $rel);
	my $destination = $cache{$rel}{'destination'};
	unless ($destination) {
		warn "no destination specified for $rel\n";
		system 'rm', '-vf', $cachefile, $metafile;
		delete $cache{$rel};
		next;
	}
	unless (-e $cachefile) {
		warn "no cached file for $rel\n";
		system 'rm', '-vf', $metafile;
		delete $cache{$rel};
		next;
	}
	unless (-e $metafile) {
		warn "no meta file for $rel\n";
		system 'rm', '-vf', $cachefile;
		delete $cache{$rel};
		next;
	}

	if (-e $backupfile) {
		if (!-e $cachefile) {
			warn "backup exists but no cached file for $rel\n";
			$cache{$rel}{'time'}[0] = -2;
		} elsif (!-e $basefile) {
			print STDERR "base file for $rel no longer exists";
			if (-s $backupfile == 0) {
				if ($dev) {
					print STDERR ", not deleting $destination\n";
				} else {
					print STDERR "\n";
					if (system('rm', '-vf', $destination) == 0) {
						system 'rm', '-vf', $backupfile, $cachefile, $metafile;
					}
				}
			} elsif (-t STDIN && -t STDERR) {
				ReadMode 'cbreak';
				print STDERR ', restore backup? [yN] ';
				my $c;
				do {
					$c = ReadKey 0;
				} while ($c ne '' && lc $c ne 'y' && lc $c ne 'n');
				ReadMode 'restore';
				print STDERR "\n";
				if ($c eq 'y') {
					if ($dev) {
						print STDERR ", not restoring $backupfile to $destination\n";
					} else {
						system 'mkdir', '-vp', dirname $destination;
						if (system('mv', '-v', $backupfile, $destination) == 0) {
							system 'rm', '-vf', $cachefile, $metafile;
						}
					}
				}
			}
		} elsif (!-e $destination) {
			warn "$destination went missing\n";
		} else {
			my $c = compare($destination, $cachefile);
			if (!$dev && $c == 1) {
				warn "$destination modified\n";
				$cache{$rel}{'time'}[0] = -1;
			} elsif ($c == -1) {
				warn "couldn't check $destination for modifications: $!\n";
				$cache{$rel}{'time'}[0] = -1;
			}
		}
	}
}

{
	my @dirs = ('');
	my $meta = slurp($metafile) or die "slurp failed for $metafile: $1\n";

	for my $dir (@dirs) {
		my $fulldir = catdir($base, $dir);
		opendir my $dh, $fulldir or do { warn "can't open dir $dir: $!\n"; next; };
		while (my $file = readdir $dh) {
			my $rel = $dir eq '' ? $file : catfile($dir, $file);
			my $full = catfile($fulldir, $file);
			next if -d $full && ($file =~ m@^\.{1,2}$@ || $full eq $cache || $full eq $bin || $file eq '.git');
			next if !-d $full && ($file eq 'links' || $file eq 'links.local');
			my $dependtime = $metatime;
			if (exists $cache{$rel}) {
				for my $depends (map { glob $_ } @{$cache{$rel}{'depends'}}) {
					$depends = catfile($base, $depends) unless file_name_is_absolute($depends);
					my $time = (stat $depends)[9];
					$dependtime = $time unless $time <= $dependtime;
				}
			}
			next if !$dev && exists $cache{$rel} &&
			        !$cache{$rel}{'ignore-times'} &&
			            ($cache{$rel}{'time'}[0] == -1 ||
			            ($cache{$rel}{'time'}[0] > 0 && $cache{$rel}{'time'}[1] > 0 &&
			             $cache{$rel}{'time'}[0] >= max((stat $full)[9], $metatime, $dependtime)));

			if (-d $full) {
				push @dirs, $rel;
			} elsif (-s $full >= 8) {
				open my $fh, '<', $full or do { warn "can't open file $rel: $!\n"; next; };
				if (read $fh, my $bytes, 8) {
					if ($bytes =~ m@^(?:\xEF\xBB\xBF)?<\?\s@) {
						my $out = catfile($cache, $rel);
						my @args = ('-e', $meta, '-o', $out, '-s', "output='$out'", $full);
						push @args, '-s', "'$hostname'";
						push @args, '-s', "base='$base'";
						push @args, map { ('-s', "'$_'") } @{$tags{$hostname}} if exists $tags{$hostname};
						system 'mkdir', '-vp', dirname $out;
						Text::PerlPP->new->Main(\@args);
					}
				} else {
					warn "can't read file $rel: $!\n";
				}
				close $fh;
			}
		}
		closedir $dh;
	}
}

{
	my @dirs = ('');

	for my $dir (@dirs) {
		my $fulldir = catdir($cache, $dir);
		opendir my $dh, $fulldir or do { warn "can't open dir $dir: $!\n"; next; };
		while (my $file = readdir $dh) {
			my $rel = $dir eq '' ? $file : catfile($dir, $file);
			my $full = catfile($fulldir, $file);
			next if -d $full && ($file =~ m@^\.{1,2}$@ || $full eq $backup || $file eq '.git');
			next if !-d $full && ($file eq 'links' || $rel eq 'PerlPP.pm' || $rel eq 'meta.pl' || $rel eq 'tags');

			if (-d $full) {
				push @dirs, $rel;
			} elsif ($rel =~ m@^(.*)\.meta$@) {
				update_cache($1, $full);
			}
		}
		closedir $dh;
	}
}

my %skipped;

for my $rel (keys %cache) {
	my $basefile = catfile($base, $rel);
	my $cachefile = catfile($cache, $rel);
	my $metafile = "$cachefile.meta";
	my $backupfile = catfile($backup, $rel);
	my $destination = $cache{$rel}{'destination'};
	my $install = $cache{$rel}{'install'} // 1;
	unless ($destination) {
		warn "no destination specified for $rel\n";
		system 'rm', '-vf', $cachefile, $metafile;
		delete $cache{$rel};
		next;
	}
	unless ($install) {
		$skipped{$rel} = 1;
		next;
	}
	unless (-e $cachefile) {
		warn "no cached file for $rel\n";
		system 'rm', '-vf', $metafile;
		delete $cache{$rel};
		next;
	}
	unless (-e $metafile) {
		warn "no meta file for $rel\n";
		system 'rm', '-vf', $cachefile;
		delete $cache{$rel};
		next;
	}

	if (-e $backupfile) {
		if ($cache{$rel}{'time'}[0] == -2 && -e $cachefile) {
			warn "new cached file created for $rel, please install manually\n";
			next;
		}
	} else {
		system 'mkdir', '-vp', dirname $backupfile;
		if (-e $destination && !-l $destination) {
			my @cmd = ('mv', '-v', $destination, $backupfile);
			if (exists $cache{$rel}{'user'} || exists $cache{$rel}{'group'}) {
				unshift @cmd, @root;
			}
			if ($dev) {
				print 'would run ' . pp(\@cmd) . "\n";
			} elsif (system(@cmd) != 0) {
				warn "failed to backup $destination\n";
				next;
			}
		} else {
			touch($backupfile) or do {
				warn "touch failed for $backupfile: $!\n";
				next;
			}
		}
	}

	if ($cache{$rel}{'time'}[0] != -1 && (!-e $destination || $cache{$rel}{'time'}[0] < (stat $cachefile)[9])) {
		if (-e $destination) {
			my $c = compare($destination, $cachefile);
			if ($c == -1) {
				warn "couldn't check $destination for modifications: $!\n";
				$skipped{$rel} = 1;
			}
			next unless $c == 1;
		}

		my @cmd = ('install', '-vD');
		if (exists $cache{$rel}{'mode'}) {
			push @cmd, '-m' . sprintf('0%o', $cache{$rel}{'mode'});
		} else {
			push @cmd, '-m0644';
		}
		if (exists $cache{$rel}{'user'} || exists $cache{$rel}{'group'}) {
			unshift @cmd, @root;
			if (exists $cache{$rel}{'user'}) {
				push @cmd, '-o', $cache{$rel}{'user'};
			}
			if (exists $cache{$rel}{'group'}) {
				push @cmd, '-g', $cache{$rel}{'group'};
			}
		}
		if ($dev) {
			print 'would run ' . pp([@cmd, $cachefile, $destination]) . "\n";
		} else {
			system @cmd, $cachefile, $destination;
		}
		if (exists $cache{$rel}{'post'}) {
			push @post, @{$cache{$rel}{'post'}};
		}
	}
}

if (%skipped) {
	print 'skipped: ' . join(' ', keys %skipped) . "\n";
}

my $root = join ' ', @root;
my %post_seen;
for my $post (@post) {
	next if exists $post_seen{$post};
	$post_seen{$post} = 1;

	$post =~ m@^(#\s*)?(.*)$@;
	my $cmd = ($1 ? "$root " : '') . $2;

	if ($dev) {
		print "would run '$cmd'\n";
	} else {
		print "- $cmd\n";
		system $cmd;
	}
}

if (-r $linkscache) {
	open my $fh, '<', $linkscache or die "can't open $linkscache for reading: $!\n";
	while (my $line = <$fh>) {
		if ($line =~ m@^\s*(\S(?:.*\S)?)\s*$@) {
			$links{$1} = 1;
		} else {
			warn "invalid line in $linkscache: $line\n";
			next;
		}
	}
	close $fh;
}

my @dirs = ($base);
while (@dirs) {
	my $dir = shift @dirs;
	opendir my $dh, $dir or die "can't open dir $dir: $!\n";
	while (my $file = readdir $dh) {
		my $full = catfile($dir, $file);
		next if -d $full && ($file =~ m@^\.{1,2}$@ || $full eq $bin || $full eq $cache || $file eq '.git');
		next if !-d $full && $file ne 'links' && $file ne 'links.local';

		if (-d $full) {
			push @dirs, $full;
		} elsif ($file eq 'links' || $file eq 'links.local') {
			update_links($full);
		}
	}
	closedir $dh;
}

if ($dev) {
	for my $link (sort keys %links) {
		if ($links{$link}) {
			print "rm -v '$link'\n"
		}
	}
} else {
	open my $fh, '>', $linkscache or warn "can't open $linkscache for writing: $!\n";
	for my $link (sort keys %links) {
		if ($links{$link}) {
			system 'rm', '-v', $link;
		} else {
			print $fh "$link\n" if $fh;
		}
	}
	close $fh if $fh;
}

sub update {
	if ($dev) {
		print "not updating config...\n";
		return 1;
	}

	print "updating config...\n";

	chdir $base or die "can't cd to $base: $!\n";
	if (system('git', 'pull', '--rebase') != 0) {
		return 0 unless -d catdir('.git', 'rebase-apply');
		if (-t STDIN && -t STDOUT) {
			print "\nExit when done.\n\n";
			system 'bash', '-c', 'bash --rcfile /etc/bash.bashrc --rcfile ~/.bashrc --rcfile <(echo PS1=\'\(update\)\ $PS1\') -i';

			return 0 if -d catdir('.git', 'rebase-apply');
		} else {
			system 'git', 'rebase', '--abort';
			return 0;
		}
	}

	return 1;
}

sub update_links {
	my ($file, $base) = @_;
	$base = abs_path($ENV{'HOME'} // getcwd()) unless defined $base;
	my $dir = abs_path dirname $file;

	open my $fh, '<', $_[0] or do { warn "can't open $file for reading: $!\n"; return 0; };
	while (my $line = <$fh>) {
		$line =~ s@^([^#]*)#.*$@$1@;
		next if $line =~ m@^\s*$@;

		my ($target, $reltarget, $link);

		if ($line =~ m@^\s*(\S(?:.*?\S)?)\s*<-\s*(\S(?:.*\S)?)\s*$@) {
			$target = $1;
			$link = $2;
		} elsif ($line =~ m@^\s*(\S(?:.*\S)?)\s*->\s*(\S(?:.*?\S)?)\s*$@) {
			$target = $2;
			$link = $1;
		} elsif ($line =~ m@^\s*(\S(?:.*\S)?)\s*$@) {
			$target = $1;
			$link = $1;
		} else {
			warn "invalid line in $file: $line\n";
			next;
		}

		$target = _abs(rel2abs($target, $dir));
		$link =~ s@/+$@'/' . basename $target@e;
		$link =~ s@^~([^/]*)@$1 ? (getpwnam($1))[7] // '~' . $1 : $ENV{HOME} // (getpwuid($<))[7]@e;
		$link =~ s@\$([a-zA-Z_][a-zA-Z0-9_]*)@$ENV{$1} // ''@ge;
		$link = _abs(rel2abs($link, $base));
		$reltarget = abs2rel($target, dirname $link);

		$links{$link} = 0;

		if (!-e $target) { warn "$reltarget doesn't exist\n"; next; }
		if (!-r $target) { warn "can't read $reltarget\n"; next; }
		if (-l $link) {
			next if readlink $link eq $reltarget;

			print STDERR "link $link exists but points to " . readlink $link;
			if (!$dev && -t STDIN && -t STDERR) {
				ReadMode 'cbreak';
				print STDERR ', replace? [yN] ';
				my $c;
				do {
					$c = ReadKey 0;
				} while ($c ne '' && lc $c ne 'y' && lc $c ne 'n');
				ReadMode 'restore';
				print STDERR "\n";
				next if $c eq '' || lc $c eq 'n';
			} else {
				print STDERR "\n";
			}
		} elsif (-e $link) { warn "$link exists and is not a link\n"; next; }

		if ($dev) {
			print "mkdir -vp '" . dirname($link) . "'\n";
			print "ln -vsfn '$reltarget' '$link'\n";
		} else {
			system 'mkdir', '-vp', dirname $link;
			system 'ln', '-vsfn', $reltarget, $link;
		}
	}
	close $fh;

	return 1;
}
