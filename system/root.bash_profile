<?
use File::Spec::Functions qw(catfile);
my $dir = (getpwnam 'root')[7];
meta {
	'destination' => catfile($dir, '.bash_profile'),
	'user' => 'root',
	'group' => 'wheel',
	'mode' => 0640,
	'post' => ['# chgrp wheel ' . $dir]
};
?>
[[ -e ~/.bashrc ]] && source ~/.bashrc
