<?
use File::Spec::Functions qw(catfile);
my $dir = (getpwnam 'root')[7];
meta {
	'destination' => catfile($dir, '.bashrc'),
	'user' => 'root',
	'group' => 'wheel',
	'mode' => 0640,
	'post' => ['# chgrp wheel ' . $dir]
};
?>
[[ -z $XDG_CONFIG_HOME ]] && export XDG_CONFIG_HOME=$HOME/.config
[[ -d /proc && $(stat -c %d:%i /) != $(stat -c %d:%i /proc/1/root/.) ]] && export CHROOT=$(cat /etc/hostname | cut -f1 -d.)

red=$(tput setaf 1)
magenta=$(tput setaf 5)
green=$(tput setaf 2)
reset=$(tput sgr0)

if [[ -z $CHROOT ]]; then
	PSHOST='\[$magenta\]\h\[$reset\]'
else
	PSHOST='\[$magenta\]$CHROOT\[$reset\] \[$red\](chroot)\[$reset\]'
fi
export PS1=$PSHOST' in \[$green\]\w\[$reset\] $([[ $? != 0 ]] && printf "\['$red'\]$?\['$reset'\]")\n\$ '

alias drop='sudo --preserve-env=CHROOT -u issytia -i'
