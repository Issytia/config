#line 1 "cache/meta.pl"

use File::Basename qw(dirname);
use File::Slurp qw(write_file);
use TOML qw(to_toml);
use POSIX qw(uname);

$S{"__arch"} = (uname)[4];

sub meta {
	my %data = ref($_[0]) eq 'HASH' ? %{$_[0]} : @_;
	if (exists $data{'install'}) {
		$data{'install'} = $data{'install'} ? \1 : \0
	}
	my $file = $S{"output"} . '.meta';
	system 'mkdir', '-vp', dirname $file;
	write_file($file, to_toml(\%data));
}

sub executable {
	system("which '$_[0]' > /dev/null 2>&1") == 0
}
